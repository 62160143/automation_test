<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Appointment Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>c608b183-43ca-4d52-b300-937a5d7ebf8d</testSuiteGuid>
   <testCaseLink>
      <guid>05485444-0c33-4b94-8058-4cf2367832ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>135121ae-d929-4641-915c-25015d2bb261</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login_Fail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>239f30eb-6c3a-4d2b-b70a-6a7e48c2660a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ChangPassword_Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d77533f-c673-433c-a9e0-19af14f988da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ChangPassword_OutOfRangeLess8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a593c5e9-0ef6-47d2-97e9-259da780279d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ChangPassword_NoSymbol</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9afb6e41-0147-4c95-a288-e5391257e3cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ChangPassword_NoNumber</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ee7e45b-e9bb-4d90-bf63-909f38a0c262</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ChangePassword_NoCharacters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34d93287-f746-432e-a06d-e6167cb50d4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout_Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
