import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable
import org.junit.Assert

import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://myid.buu.ac.th/')

WebUI.setText(findTestObject('Object Repository/Page_My ID/input_(Username)_user'), '62160143')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_My ID/input_(Password)_pass'), 'nDuwwK0hhdRIc2WF7bU4uw==')

WebUI.click(findTestObject('Object Repository/Page_My ID/button_Sign in'))

message = WebUI.getText(findTestObject('Object Repository/Page_My ID/h3_(Profile)'))
Assert.assertEquals('ข้อมูลส่วนบุคคล (Profile)', message)

message1 = WebUI.getText(findTestObject('Object Repository/Page_My ID/td_(Citizen ID)134970025'))
Assert.assertEquals('หมายเลขบัตรประชาชน (Citizen ID)\n134970025****', message1)


message2 = WebUI.getText(findTestObject('Object Repository/Page_My ID/td_(Student code)          62160143'))
Assert.assertEquals('รหัสนิสิต (Student code)\n62160143', message2)

message3 = WebUI.getText(findTestObject('Object Repository/Page_My ID/td_-  (Name)          Supakrit Kongkam'))
Assert.assertEquals('ชื่อ - นามสกุล (Name)\nSupakrit Kongkam', message3)

message4 = WebUI.getText(findTestObject('Object Repository/Page_My ID/td_(Faculty)Informatics'))
Assert.assertEquals('คณะ (Faculty)\nInformatics', message4)

message5 = WebUI.getText(findTestObject('Object Repository/Page_My ID/td_(Password Expire)2022-07-03 095314 (bala_2bdd29'))
Assert.assertEquals('รหัสผ่านหมดอายุ (Password Expire)\n2022-07-03 09:53:14 (balance : 111 days)', message5)

message6 = WebUI.getText(findTestObject('Object Repository/Page_My ID/td_(Account Expire)2572-05-28 020120'))
Assert.assertEquals('บัญชีผู้ใช้หมดอายุ (Account Expire)\n2572-05-28 02:01:20', message6)

WebUI.closeBrowser()

